package Proxy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

class ProxyExample {

	/**
	 * Test method
	 */
	public static void main(String[] args) {
		ArrayList<Image> list = new ArrayList<Image>();
		Scanner sc = new Scanner(System.in);
		int command = 0, i = 0;

		final Image IMAGE1 = new ProxyImage("HiRes_10MB_Photo1");
		final Image IMAGE2 = new ProxyImage("HiRes_10MB_Photo2");
		final Image IMAGE3 = new ProxyImage("HiRes_10MB_Photo3");
		final Image IMAGE4 = new ProxyImage("HiRes_10MB_Photo4");
		final Image IMAGE5 = new ProxyImage("HiRes_10MB_Photo5");

		list.addAll(Arrays.asList(IMAGE1, IMAGE2, IMAGE3, IMAGE4, IMAGE5));

		System.out.println("Valokuvat:");
		for (Image img : list) {
			img.showData();
		}

		System.out.println("\nKäynnistetään  kuvien selausohjelma");
		System.out.println("1 = eteen, 2 = taakse, 3 = lopeta\n");

		list.get(i).displayImage();
		do {

			System.out.println("\nanna komento");
			command = sc.nextInt();

			switch (command) {
			case 1:
				if (i < list.size() - 1) {
					i++;
				} else {
					i = 0;
				}
				list.get(i).displayImage();
				break;
			case 2:
				if (i > 0) {
					i--;
				} else {
					i = list.size() - 1;
				}
				list.get(i).displayImage();
				break;

			}
		} while (command != 3);
		System.out.println("Quitting...");

	}

}
