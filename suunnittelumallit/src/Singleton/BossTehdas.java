package Singleton;

public class BossTehdas implements Factory_IF {

	private BossTehdas() {
	};

	private static BossTehdas firstInstance = null;

	public static synchronized BossTehdas getInstance() {
		if (firstInstance == null) {
			firstInstance = new BossTehdas();
		}
		return firstInstance;
	}

	@Override
	public Keng�t luoKeng�t() {
		// TODO Auto-generated method stub
		return new BossKeng�t();
	}

	@Override
	public Paita luoPaita() {
		// TODO Auto-generated method stub
		return new BossPaita();
	}

	@Override
	public Lippis luoLippis() {
		// TODO Auto-generated method stub
		return new BossLippis();
	}

	@Override
	public Farkut luoFarkut() {
		// TODO Auto-generated method stub
		return new BossFarkut();
	}


}