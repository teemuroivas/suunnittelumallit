package Singleton;

public class Main {

	public static void main(String[] args) {

//		BossTehdas bt = new BossTehdas();
		BossTehdas bt = BossTehdas.getInstance();
		BossTehdas bt2 = BossTehdas.getInstance();
		System.out.println("Onko samat? "+bt+" ja "+bt2);
		AdidasTehdas at = AdidasTehdas.getInstance();
		Insinoori ins1 = new Insinoori(bt);
		ins1.luetteleVaatteet();
		Insinoori ins2 = new Insinoori(at);
		ins2.luetteleVaatteet();
	}

}
