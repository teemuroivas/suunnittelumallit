package Singleton;

public class AdidasTehdas implements Factory_IF{

	private AdidasTehdas() {
	};

	private static AdidasTehdas firstInstance = null;

	public static synchronized AdidasTehdas getInstance() {
		if (firstInstance == null) {
			firstInstance = new AdidasTehdas();
		}
		return firstInstance;
	}

	@Override
	public Farkut luoFarkut() {
		// TODO Auto-generated method stub
		return new AdidasFarkut();
	}

	@Override
	public Keng�t luoKeng�t() {
		// TODO Auto-generated method stub
		return new AdidasKeng�t();
	}

	@Override
	public Paita luoPaita() {
		// TODO Auto-generated method stub
		return new AdidasPaita();
	}

	@Override
	public Lippis luoLippis() {
		// TODO Auto-generated method stub
		return new AdidasLippis();
	}


}