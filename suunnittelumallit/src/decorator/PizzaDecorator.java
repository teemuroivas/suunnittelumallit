package decorator;

public abstract class PizzaDecorator implements Pizza {
	protected Pizza PizzaToBeDecorated;

	public PizzaDecorator(Pizza PizzaToBeDecorated) {
		this.PizzaToBeDecorated = PizzaToBeDecorated;
	}

	public String getDescription() {
		return PizzaToBeDecorated.getDescription();
	}

	public double getPizzaPrice() {
		return PizzaToBeDecorated.getPizzaPrice();
	}

}
