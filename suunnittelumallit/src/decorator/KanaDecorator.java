package decorator;

public class KanaDecorator extends PizzaDecorator {
	private Double hinta = 2.5;

	public KanaDecorator(Pizza PizzaToBeDecorated) {
		super(PizzaToBeDecorated);
	}

	@Override
	public String getDescription() {
		return super.getDescription() + ", kana";
	}

	@Override
	public double getPizzaPrice() {
		return super.getPizzaPrice() + getHinta();
	}

	public double getHinta() {
		return hinta;
	}
}
