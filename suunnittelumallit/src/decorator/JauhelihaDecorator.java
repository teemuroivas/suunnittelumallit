package decorator;

public class JauhelihaDecorator extends PizzaDecorator {
	private Double hinta = 2.0;

	public JauhelihaDecorator(Pizza PizzaToBeDecorated) {
		super(PizzaToBeDecorated);
	}

	@Override
	public String getDescription() {
		return super.getDescription() + ", jauheliha";
	}

	@Override
	public double getPizzaPrice() {
		return super.getPizzaPrice() + getHinta();
	}

	public double getHinta() {
		return hinta;
	}
}