package decorator;

public class AurajuustoDecorator extends PizzaDecorator {
	private Double hinta = 1.5;

	public AurajuustoDecorator(Pizza PizzaToBeDecorated) {
		super(PizzaToBeDecorated);
	}

	@Override
	public String getDescription() {
		return super.getDescription() + ", aurajuusto";
	}

	public double getHinta() {
		return hinta;
	}

	@Override
	public double getPizzaPrice() {
		return super.getPizzaPrice() + getHinta();
	}

}
