package decorator;

class Pohja implements Pizza {
	private Double hinta = 4.0;

	public String getDescription() {
		return "pohja";
	}

	public double getHinta() {
		return hinta;
	}

	@Override
	public double getPizzaPrice() {
		return getHinta();
	}
}
