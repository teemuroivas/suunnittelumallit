package decorator;

public class DecoratedPizzaTest {
	public static void main(String[] args) {

		System.out.println("Menu:");

		Pizza decoratedPizza1 = new TomaattiDecorator(new KanaDecorator(new Pohja()));
		System.out.println(decoratedPizza1.getDescription() + " - " + decoratedPizza1.getPizzaPrice() + "�");

		Pizza decoratedPizza2 = new TomaattiDecorator(new JauhelihaDecorator(new KanaDecorator(new Pohja())));
		System.out.println(decoratedPizza2.getDescription() + " - " + decoratedPizza2.getPizzaPrice() + "�");

		Pizza decoratedPizza3 = new TomaattiDecorator(new AurajuustoDecorator(new Pohja()));
		System.out.println(decoratedPizza3.getDescription() + " - " + decoratedPizza3.getPizzaPrice() + "�");

	}
}
