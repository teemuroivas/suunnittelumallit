package decorator;

public class TomaattiDecorator extends PizzaDecorator {
	private Double hinta = 1.0;

	public TomaattiDecorator(Pizza PizzaToBeDecorated) {
		super(PizzaToBeDecorated);
	}

	@Override
	public String getDescription() {
		return super.getDescription() + ", tomaatti";
	}

	@Override
	public double getPizzaPrice() {
		return super.getPizzaPrice() + getHinta();
	}

	public double getHinta() {
		return hinta;
	}
}
