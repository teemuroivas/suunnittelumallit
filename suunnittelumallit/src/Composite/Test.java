package Composite;

public class Test {

		public static void main(String[] args){

			Komponentti kotelo = new Kotelo();
			Komponentti emo = new Emolevy();
			Komponentti nayttis = new Nayttis();
			Komponentti prossu = new Prosessori();
			Komponentti muisti = new Muisti();

			emo.liita(nayttis);
			emo.liita(prossu);
			kotelo.liita(emo);
			kotelo.liita(muisti);

			System.out.println("Kokoonpanon hinta on "+ kotelo.hinta());
			//virheellinen yritys
			nayttis.liita(emo);


	}
}
