package Composite;

import java.util.ArrayList;

public class Kotelo implements Komponentti {
	ArrayList<Komponentti> komponentit = new ArrayList();

	double hinta = 30;

	public double hinta() {
		for (Komponentti k : komponentit) {
			hinta = hinta + k.hinta();
		}
		return hinta;
	}

	@Override
	public void liita(Komponentti k) {
		// TODO Auto-generated method stub
		komponentit.add(k);

	}

}
