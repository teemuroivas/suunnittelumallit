package Observer;

import java.util.Observable;
import java.util.Observer;

public class Subject extends Observable {

	public void attach(Observer o) {
		addObserver(o);
	}

	public void detach(Observer o) {
		deleteObserver(o);
	}

	protected void notifyobs() {
		notifyObservers();
	}

}
