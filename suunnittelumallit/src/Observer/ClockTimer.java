package Observer;

public class ClockTimer extends Subject implements Runnable {

	public int hour = 12, minute = 0, second = 55;

	void tick() {
		second++;
		if (second == 60) {
			second = 0;
			minute++;
			if (minute == 60) {
				minute = 0;
				hour++;
				if (hour == 24) {
					hour = 0;
					hour++;
				}
			}
		}
	}

	@Override
	public void run() {

		while (true) {
			tick();
			setChanged();
			notifyobs();
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public int getHour() {
		return hour;
	}

	public int getMinute() {
		return minute;
	}

	public int getSecond() {
		return second;
	}

}
