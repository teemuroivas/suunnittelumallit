package Observer;

import java.util.Observable;
import java.util.Observer;

public class DigitalClock implements Observer {
	private ClockTimer timer;
	String hh, mm, ss;

	public DigitalClock(ClockTimer ct) {
		timer = ct;
		timer.attach(this);
	}

	private void draw() {
		int hour = timer.getHour();
		int minute = timer.getMinute();
		int second = timer.getSecond();

		hh = formatTime(hour);
		mm = formatTime(minute);
		ss = formatTime(second);

		System.out.println(hh + ":" + mm + ":" + ss);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg0 == timer) {
			draw();
		}
	}

	public String formatTime(int value) {
		String time;
		if (value < 10) {
			time = "0" + value;
		} else {
			time = String.valueOf(value);
		}
		return time;

	}
}