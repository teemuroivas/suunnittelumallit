package abstractFactory;

public class Main {

	public static void main(String[] args)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {

		Tehdas tehdas = null;
		Class c = null;

		c = Class.forName("abstractFactory.BossTehdas");
		tehdas = (Tehdas) c.newInstance();

		Insinoori insinoori = new Insinoori(tehdas);
		insinoori.luetteleVaatteet();

		c = Class.forName("abstractFactory.AdidasTehdas");
		tehdas = (Tehdas) c.newInstance();

		Insinoori ins = new Insinoori(tehdas);
		ins.luetteleVaatteet();

//		BossTehdas bt = new BossTehdas();
//		AdidasTehdas at = new AdidasTehdas();
//		Insinoori ins1 = new Insinoori(bt);
//		ins1.luetteleVaatteet();
//		Insinoori ins2 = new Insinoori(at);
//		ins2.luetteleVaatteet();
	}

}
