package abstractFactory;

public class Insinoori {

	private Farkut farkut = null;
	private Keng�t keng�t = null;
	private Paita paita = null;
	private Lippis lippis = null;

	public Insinoori(Tehdas tehdas) {

		farkut = tehdas.luoFarkut();
		keng�t = tehdas.luoKeng�t();
		paita = tehdas.luoPaita();
		lippis = tehdas.luoLippis();

	}

	public void luetteleVaatteet() {
		System.out.println("Puettuna: " + farkut + ", " + keng�t + ", " + paita + " ja " + lippis);
	}

}