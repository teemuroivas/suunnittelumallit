package State;

public class TestStates {

	public static void main(String[] args) {

		CharmanderState charmander = new CharmanderState();
		CharmeleonState charmeleon = new CharmeleonState();
		CharizardState charizard = new CharizardState();

		PokemonContext pokemon = new PokemonContext(charmander);

		pokemon.getState().roar();
		pokemon.getState().attack();
		System.out.println("Health: " + pokemon.getState().getHealth());

		System.out.println("");
		pokemon.setState(charmeleon);
		pokemon.getState().roar();
		pokemon.getState().attack();
		System.out.println("Health: " + pokemon.getState().getHealth());

		System.out.println("");
		pokemon.setState(charizard);
		pokemon.getState().roar();
		pokemon.getState().attack();
		System.out.println("Health: " + pokemon.getState().getHealth());

	}
}
