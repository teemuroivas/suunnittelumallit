package State;

public class CharmanderState implements State_IF {
	private int health = 40;

	public CharmanderState() {

	}

	@Override
	public void attack() {
		// TODO Auto-generated method stub
		System.out.println("Attack: Swipe");

	}

	@Override
	public void roar() {
		// TODO Auto-generated method stub
		System.out.println("Roar!");
	}

	public int getHealth() {
		return health;
	}

	@Override
	public String toString() {
		return "Pokemon is now in charmander state";

	}

}
