package State;

public class CharizardState implements State_IF {
	private int health = 120;

	public CharizardState() {

	}

	@Override
	public void attack() {
		// TODO Auto-generated method stub
		System.out.println("Attack: BURN EVERYTHING");

	}

	@Override
	public void roar() {
		// TODO Auto-generated method stub
		System.out.println("RRRRRROOOOOOOOOOOOOOOAAAAARRRRRRRRRRRRRRR!");
	}

	public int getHealth() {
		return health;
	}

	@Override
	public String toString() {
		return "Pokemon is now in charizard state";

	}
}
