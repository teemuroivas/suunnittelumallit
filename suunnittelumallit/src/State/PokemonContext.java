package State;

public class PokemonContext {
	private State_IF state;

	public PokemonContext(State_IF state) {
		this.state = state;
		System.out.println(state.toString());
	}

	public void setState(State_IF state) {
		this.state = state;
		System.out.println(state.toString());
	}

	public State_IF getState() {
		return state;
	}
}
