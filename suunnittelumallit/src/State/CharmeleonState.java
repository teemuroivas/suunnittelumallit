package State;

public class CharmeleonState implements State_IF {
	private int health = 80;

	public CharmeleonState() {

	}

	@Override
	public void attack() {
		// TODO Auto-generated method stub
		System.out.println("Attack: Fireball");

	}

	@Override
	public void roar() {
		// TODO Auto-generated method stub
		System.out.println("RROARR!");
	}

	public int getHealth() {
		return health;
	}

	@Override
	public String toString() {
		return "Pokemon is now in charmeleon state";

	}
}
