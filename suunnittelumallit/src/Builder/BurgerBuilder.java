package Builder;

interface BurgerBuilder {

	public void printBurger();

	public void buildSa();

	public void buildKa();

	public void buildLe();

	public void buildPi();
}
