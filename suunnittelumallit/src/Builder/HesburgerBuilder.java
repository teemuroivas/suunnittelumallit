package Builder;

/* "ConcreteBuilder 1" */
class HesburgerBuilder implements BurgerBuilder {
	String burger = "";

	@Override
	public void buildSa() {
		// TODO Auto-generated method stub
		burger = burger + new Salaatti().name() + " ";

	}

	@Override
	public void buildKa() {
		// TODO Auto-generated method stub
		burger = burger + new Kastike().name() + " ";
	}

	@Override
	public void buildLe() {
		// TODO Auto-generated method stub
		burger = burger + new HampurilaisLeipä().name() + " ";
	}

	@Override
	public void buildPi() {
		// TODO Auto-generated method stub
		burger = burger + new Pihvi().name() + " ";
	}

	@Override
	public void printBurger() {
		System.out.println("Burgeri koostuu: " + burger);
	}

}
