package Builder;

class Asiakas {
	/* A customer ordering a pizza. */

	public static void main(String[] args) {
		Director d = new Director();
		BurgerBuilder hb = new HesburgerBuilder();
		BurgerBuilder mb = new McDonaldsBuilder();
		d.setBurgerBuilder(hb);
		d.constructBurger();
		d.getB();
		d.setBurgerBuilder(mb);
		d.constructBurger();
		d.getB();
	}

}
