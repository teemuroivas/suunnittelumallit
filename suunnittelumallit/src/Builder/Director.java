package Builder;

public class Director {

	private BurgerBuilder bb;

	public void setBurgerBuilder(BurgerBuilder bb) {
		this.bb = bb;
	}

	public void constructBurger() {
		bb.buildSa();
		bb.buildKa();
		bb.buildLe();
		bb.buildPi();
	}

	public void getB() {
		System.out.println(bb);
		bb.printBurger();
	}

}
