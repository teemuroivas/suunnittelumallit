package Builder;

import java.util.ArrayList;

class McDonaldsBuilder implements BurgerBuilder {
	ArrayList<Item> burger = new ArrayList();

	@Override
	public void buildSa() {
		// TODO Auto-generated method stub
		burger.add(new Salaatti());
	}

	@Override
	public void buildKa() {
		// TODO Auto-generated method stub
		burger.add(new Kastike());
	}

	@Override
	public void buildLe() {
		// TODO Auto-generated method stub
		burger.add(new HampurilaisLeipä());
	}

	@Override
	public void buildPi() {
		// TODO Auto-generated method stub
		burger.add(new Pihvi());
	}

	@Override
	public void printBurger() {
		// TODO Auto-generated method stub
		System.out.print("Burgeri koostuu: ");
		for (Item item : burger) {
			System.out.print(item.name() + " ");
		}
	}

}
