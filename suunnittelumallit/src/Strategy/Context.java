package Strategy;

import java.util.List;

public class Context {
	private ListConverter listconverter;

	public Context(ListConverter listconverter) {
		this.listconverter = listconverter;
	}

	public String muunna(List list) {
		return listconverter.listToString(list);
	}

	public void setStrategy(ListConverter listconverter) {
		this.listconverter = listconverter;
	}
}
