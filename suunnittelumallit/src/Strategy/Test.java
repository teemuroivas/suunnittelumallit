package Strategy;

import java.util.ArrayList;
import java.util.List;

public class Test {
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();

		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);
		list.add(7);
		list.add(8);
		list.add(9);

		Context context = new Context(new muuntaja_1());
		System.out.print(context.muunna(list));
		System.out.print("\n"); //selkeyttää lukemista

		context.setStrategy(new muuntaja_2());
		System.out.print(context.muunna(list));
		System.out.print("\n\n"); //selkeyttää lukemista

		context.setStrategy(new muuntaja_3());
		System.out.print(context.muunna(list));

	}
}
