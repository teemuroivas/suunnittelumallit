package Strategy;

import java.util.List;

public class muuntaja_3 implements ListConverter {

	@Override
	public String listToString(List list) {

		String s = "";
		for (int i = 0; i < list.size(); i++) {
			s = s + list.get(i).toString();
			if (i % 3 == 2) {
				s = s + "\n";
			}
		}

		return s;
	}
}
