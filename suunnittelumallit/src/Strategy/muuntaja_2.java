package Strategy;

import java.util.Iterator;
import java.util.List;

public class muuntaja_2 implements ListConverter {

	@Override
	public String listToString(List list) {
		int i = 1;
		String s = "";

		Iterator itr = list.iterator();

		while (itr.hasNext()) {
			Object obj = itr.next();
			s = s + obj.toString();
			if (i % 2 == 0) {
				s = s + "\n";
			}
			i++;
		}
		return s;
	}

}
