package adapter;

public class Test {
	public static void main(String[] args) {
		//Muuttaa suomalaisen englantilaiseksi

		PersonService service = new PersonService();
		Person person = new Person("John");
		FinnishPerson fperson = new FinnishPerson("Joonas");

		service.PrintName(person);

// 		Kommenteissa olevaa rivi� ei hyv�ksyt� sill� service ei hyv�ksy fpersonin rajapintaa, adapteri muuttaa asian.
//		service.PrintName(fperson);
		PersonAdapter pa = new PersonAdapter(fperson);
		service.PrintName(pa);

	}
}
