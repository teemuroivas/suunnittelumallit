package adapter;

public class PersonAdapter implements IPerson {
	private IFinnishPerson fp;

	public PersonAdapter(IFinnishPerson fp) {
		this.fp = fp;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return fp.getNimi();
	}

	@Override
	public void setName(String name) {
		fp.setNimi(name);;

	}
}
