package mediator;

public class Tulosrivi {

  private Hypp��j� h;
  private double yhteispisteet;

  public Tulosrivi(Hypp��j� h, double yhteispisteet) {
    this.h = h;
    this.yhteispisteet = yhteispisteet;
  }

  public Hypp��j� getH() {
    return h;
  }

  public void setH(Hypp��j� h) {
    this.h = h;
  }

  public double getYhteispisteet() {
    return yhteispisteet;
  }

  public void setYhteispisteet(double yhteispisteet) {
    this.yhteispisteet = yhteispisteet;
  }
}
