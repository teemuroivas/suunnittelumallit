package mediator;

public class Test {
  public static void main(String[] args) {
    Mediator mediator = new Mediator();
    Hypp��j� h1 = new Hypp��j�(mediator, "Seppo");
    Hypp��j� h2 = new Hypp��j�(mediator, "Jari");
    Hypp��j� h3 = new Hypp��j�(mediator, "Lari");
    Hypp��j� h4 = new Hypp��j�(mediator, "Kake");
    Hypp��j� h5 = new Hypp��j�(mediator, "Jeppe");
    Tulostaulu t = new Tulostaulu(mediator);
    Kisasihteeri sih = new Kisasihteeri();
    Mittamies m = new Mittamies();
    Tuomari t1 = new Tuomari();
    Tuomari t2 = new Tuomari();
    Tuomari t3 = new Tuomari();
    Tuomari t4 = new Tuomari();
    Tuomari t5 = new Tuomari();
    mediator.addSih(sih);
    mediator.addTulostaulu(t);
    mediator.addMittamies(m);
    mediator.addHypp��j�(h1);
    mediator.addHypp��j�(h2);
    mediator.addHypp��j�(h3);
    mediator.addHypp��j�(h4);
    mediator.addHypp��j�(h5);
    mediator.addTuomari(t1);
    mediator.addTuomari(t2);
    mediator.addTuomari(t3);
    mediator.addTuomari(t4);
    mediator.addTuomari(t5);
    h1.Hypp��();
    h1.Hypp��();
    h2.Hypp��();
    h2.Hypp��();
    h3.Hypp��();
    h3.Hypp��();
    h4.Hypp��();
    h4.Hypp��();
    h5.Hypp��();
    h5.Hypp��();
    t.n�yt�Tulokset();
  }
}
