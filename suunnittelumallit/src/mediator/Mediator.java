package mediator;

import java.util.ArrayList;

public class Mediator {

  private ArrayList<Hypp��j�> hypp��j�t = new ArrayList();
  private ArrayList<Tuomari> tuomarit = new ArrayList();
  private Tulostaulu tulostaulu;
  private Mittamies mittamies;
  private Kisasihteeri sih;

  public Mediator() {

  }

  public void addTulostaulu(Tulostaulu tulostaulu) {
    this.tulostaulu = tulostaulu;
  }

  public void addHypp��j�(Hypp��j� hypp��j�) {
    hypp��j�t.add(hypp��j�);
  }

  public void addTuomari(Tuomari tuomari) {
    this.tuomarit.add(tuomari);
  }

  public void addMittamies(Mittamies mittamies) {
    this.mittamies = mittamies;
  }

  public void addSih(Kisasihteeri sih) {
    this.sih = sih;
  }

  public void Hypp��(Hypp��j� hypp��j�) {
    ArrayList<Double> tpisteet = new ArrayList();

    for (Tuomari t : this.tuomarit) {
      tpisteet.add(t.arvosteleHyppy());
    }

    double pituus = mittamies.mittaaHyppy();
    Hyppy hyppy = new Hyppy(pituus, tpisteet);
    hypp��j�.setHyppy(hyppy);
  }

  public void n�yt�Tulostaulu() {

    for (Hypp��j� h : hypp��j�t) {
      for (Hyppy hy : h.getHypyt()) {
        double yhtpisteet = sih.laskeHypynPisteet(hy.getPisteet(), hy.getLength(), tuomarit.size());
        Tulosrivi tr = new Tulosrivi(h, yhtpisteet);
        tulostaulu.addTulos(tr);
      }
    }
    tulostaulu.printTulokset();
  }

}
