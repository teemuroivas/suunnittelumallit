package ChainOfResponsibility;

import java.io.BufferedReader;
import java.io.InputStreamReader;

class TyonJakaja {
	public static void main(String[] args) {
		Esimies esimies = new Esimies();
		Paalikko paalikko = new Paalikko();
		Toimitusjohtaja tj = new Toimitusjohtaja();

		esimies.setSuccessor(paalikko);
		paalikko.setSuccessor(tj);
		System.out.println("Palkkasi on " + Palkankorotus.PALKKA + "$");
		try {
			while (true) {
				System.out.println("Anna uusi palkkatoiveesi.");
				System.out.print(">");
				double d = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
				esimies.processRequest(new KorotusRequest(d));
			}
		} catch (Exception e) {
			System.exit(1);
		}
	}
}
