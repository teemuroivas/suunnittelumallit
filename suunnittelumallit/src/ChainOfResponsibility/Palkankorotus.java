package ChainOfResponsibility;

abstract class Palkankorotus {
	protected static double PALKKA = 2000;
	protected Palkankorotus successor;

	public void setSuccessor(Palkankorotus successor) {
		this.successor = successor;
	}

	abstract public void processRequest(KorotusRequest request);

}
