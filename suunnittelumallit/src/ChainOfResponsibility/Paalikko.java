package ChainOfResponsibility;

class Paalikko extends Palkankorotus {
	private double alinsallittu = 1.02 * PALKKA;
	private double ylinsallittu = 1.05 * PALKKA;

	public void processRequest(KorotusRequest request) {
		if (request.getAmount() <= ylinsallittu && request.getAmount() > alinsallittu) {
			System.out.println("Paalikko vahvistaa palkankorotuksen " + request.getAmount());
		} else if (successor != null) {
			successor.processRequest(request);
		}
	}
}
