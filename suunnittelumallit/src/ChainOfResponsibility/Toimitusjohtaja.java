package ChainOfResponsibility;

class Toimitusjohtaja extends Palkankorotus {
	private double sallittu = 1.05 * PALKKA;

	public void processRequest(KorotusRequest request) {
		if (request.getAmount() > sallittu) {
			System.out.println("Toimitusjohtaja vahvistaa palkankorotuksen " + request.getAmount());
		} else if (successor != null) {
			successor.processRequest(request);
		}
	}
}
