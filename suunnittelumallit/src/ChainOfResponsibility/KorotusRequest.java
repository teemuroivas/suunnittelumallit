package ChainOfResponsibility;

class KorotusRequest {
	private double amount;

	public KorotusRequest(double amount) {
		this.amount = amount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amt) {
		amount = amt;
	}

}
