package ChainOfResponsibility;

class Esimies extends Palkankorotus {
	private double Sallittu = 1.02 * PALKKA;

	public void processRequest(KorotusRequest request) {

		if (request.getAmount() <= PALKKA) {
			System.out.println("Pyynt� ei voi olla sama tai pienempi kuin nykyinen palkka");
		} else if (request.getAmount() <= Sallittu) {
			System.out.println("Esimies vahvistaa palkankorotuksen $" + request.getAmount());
		} else if (successor != null) {
			successor.processRequest(request);
		}
	}
}
