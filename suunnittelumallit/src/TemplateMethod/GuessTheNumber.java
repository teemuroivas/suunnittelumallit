package TemplateMethod;

import java.util.Scanner;

public class GuessTheNumber extends Game {
	/* Implementation of necessary concrete methods */
	Scanner scanner = new Scanner(System.in);
	private int randomNum, player, guess, min, max;
	private boolean end = false;

	void initializeGame() {
		min = 1;
		max = 100;
		randomNum = min + (int) (Math.random() * max);
		System.out.println(
				"Arvotaan numero väliltä " + min + " - " + max + ". " + "Ensimmäiseksi oikein arvannut voittaa.");
	}

	void makePlay(int player) {
		this.player = player;
		System.out.println("Pelaajan " + player + " vuoro arvata.");

		while (!scanner.hasNextInt()) {
			System.out.println("Anna numero!");
			scanner.next();
		}
		;
		guess = scanner.nextInt();
		if (guess < randomNum) {
			System.out.println("arvaus on liian pieni");
		} else if (guess > randomNum) {
			System.out.println("arvaus on liian suuri");
		} else {
			setEnd(true);
		}

	}

	boolean endOfGame() {
		return end;
	}

	void printWinner() {
		System.out.println("Pelaaja " + player + " voitti pelin!");
	}

	public void setEnd(boolean end) {
		this.end = end;
	}
}